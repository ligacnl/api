require('dotenv').config()

const express = require('express')
const helmet = require('helmet')
const cors = require('cors')
const debug = require('debug')('ligacnl:app')
const fs = require('fs')
const request = require('request')
const moment = require('moment')

const auth = require('./plugins/auth')
const database = require('./plugins/database')
const sitemap = require('./plugins/sitemap')
const APIError = require('./apierror')

const app = express()
app.use(helmet())
app.use(cors())
app.use(express.json({limit: '1mb'}))
app.use(express.urlencoded({extended: false}))
app.use(auth.validateToken)
app.use(database)

// CDN Plugin
app.set('cdn', 'https://cdn.ligacnl.com.br/')
const aws = require('aws-sdk')
const awsOptions = {
  region: process.env.AWS_REGION || 'us-east-1',
  accessKeyId: process.env.AWS_ACCESS_KEY_ID || 'AKIATH6MOQNTEQ4CIGM6',
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY ||
    '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',
  sslEnabled: process.env.NODE_ENV === 'production',
  httpOptions: {
    proxy: process.env.http_proxy,
  },
}
app.set('s3', new aws.S3(awsOptions))

// oEmbed plugin
app.set('OEMBED_PROVIDERS_URL', 'https://oembed.com/providers.json')
const oembedProvidersFile = './data/omebedProviders.json'
try {
  const providers = JSON.parse(fs.readFileSync(oembedProvidersFile))
  debug(`oEmbed retrieved from file: ${providers.length} providers.`)
  app.set('oembedProviders', providers)
} catch (err) {
  const providersUrl = app.get('OEMBED_PROVIDERS_URL')
  debug('Requesting oEmbed providers from:', providersUrl)
  request({
    method: 'GET',
    url: providersUrl,
  }, (err, response, body) => {
    if (err) return debug('Unable to retrieve oEmbed providers.')
    const providers = JSON.parse(body)
    fs.writeFile(oembedProvidersFile, body, (err) => {
      if (err) debug(`Error writing ${oembedProvidersFile}: ${err}`)
    })
    debug(`oEmbed retrieved from server: ${providers.length} providers.`)
    app.set('oembedProviders', providers)
  })
}

app.set('dateFormat', 'DD/MM/YY hh:mm:ss')

app.get('/sitemap.xml', sitemap)
app.use('/auth', require('./routes/auth'))
app.use('/players', require('./routes/players'))
app.use('/news', require('./routes/news'))
app.use('/embed', require('./routes/embed'))
app.use('/files', require('./routes/files'))
app.use('/admin', require('./routes/admin'))

app.use(function(req, res, next) {
  next(new APIError(404, 'Página não encontrada.'))
})

app.use(function(err, req, res, next) {
  err.status = err.status || 500

  debug(`[${ moment() }] Error on API call: ${ req.originalUrl } \
/ Error: ${ err.message }`)
  res.status(err.status)
  res.send({
    status: err.status,
    message: err.message,
  })
})


module.exports = app
