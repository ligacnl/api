module.exports = {
  root: true,
  env: {
    node: true,
    es6: true
  },
  extends: ["eslint:recommended", "google"],
  parserOptions: {
    ecmaVersion: 8,
    sourceType: "module"
  },
  rules: {
    "require-jsdoc": 0,
    "no-invalid-this": 0,
    "semi": [2, "never"],
    "prefer-template": 2,
    "max-len": 0
  }
};
