const debug = require('debug')('ncl:models:news')
const mongoose = require('mongoose')
const slug = require('slug')

const NewsSchema = new mongoose.Schema({
  author: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  slug: String,
  title: {type: String, required: true},
  image: String,
  description: {type: String},
  game: {type: String, default: 'all'},
  carousel: {type: Boolean, default: false},
  content: {type: String, required: true},
  tags: [{type: String}],
}, {
  timestamps: true,
})

NewsSchema.pre('save', function(next) {
  const News = mongoose.model('News')

  let i = 0
  const checkSlug = (newsSlug, slugTaken) => {
    if (!slugTaken) {
      debug('Calculated slug for news [', this._id, ']:', newsSlug)
      this.slug = newsSlug

      return next()
    } else {
      return News.findOne({slug: newsSlug}).then((newsItem) => {
        const slugTaken = !!newsItem
          && newsItem._id.toString() != this._id.toString()
        if (slugTaken) newsSlug = slug(`${this.title } ${++i}`, {lower: true})
        return checkSlug(newsSlug, slugTaken)
      })
    }
  }

  return checkSlug(slug(this.title, {lower: true}), true)
})

NewsSchema.set('toObject', {getters: true, virtuals: true})
NewsSchema.set('toJSON', {
  getters: true,
  virtuals: true,
  versionKey: false,
  transform: toJSON,
})
function toJSON(document, output, options) {
  if (document.related) output.related = document.related
  if (!document.image) {
    switch (document.game) {
      case 'lol':
        output.image = 'https://cdn.ligacnl.com.br/league_of_legends.jpg'
        break
      case 'dota2':
        output.image = 'https://cdn.ligacnl.com.br/dota2.jpg'
        break
      case 'csgo':
        output.image = 'https://cdn.ligacnl.com.br/counter_strike_global_offensive.jpg'
        break
      case 'ow':
        output.image = 'https://cdn.ligacnl.com.br/overwatch.jpg'
        break
      default:
        output.image = 'https://cdn.ligacnl.com.br/ligacnl.jpg'
        break
    }
  }
}

module.exports = mongoose.model('News', NewsSchema)
