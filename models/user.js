// const debug = require('debug')('ncl:models:users')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const request = require('request')

const SALT_WORK_FACTOR = 10

const UserSchema = new mongoose.Schema({
  email: {type: String, unique: true, lowercase: true},
  password: {type: String},
  name: String,
  region: String,
  summonerName: {type: String},
  picture: {
    contentType: String,
    data: Buffer,
  },
  steamId: {type: String},
  facebookId: String,
  riotId: String,
  refuseInvite: {type: Boolean, default: false},
  admin: {type: Boolean, default: false},
  organizer: {type: Boolean, default: false},
  editor: {type: Boolean, default: false},
  bot: {type: Boolean, default: false},
  emailVerified: {type: Boolean, default: false},
  resetToken: String,
})

UserSchema.virtual('picture.url').get(function() {
  return `/players/${this._id}/avatar`
})

UserSchema.set('toJSON', {virtuals: true, versionKey: false, transform: toJSON})
function toJSON(document, output, options) {
  if (output.picture && output.picture.data) delete output.picture.data
  delete output.password
  delete output.resetToken
}

UserSchema.pre('save', function(next) {
  const user = this
  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next()
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return next(err)
    // hash the password along with our new salt
    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return next(err)
      // override the cleartext password with the hashed one
      user.password = hash
      next()
    })
  })
})

UserSchema.methods.checkPassword = function(candidatePassword) {
  return new Promise((resolve, reject) => {
    if (!this.password) return reject(new Error('Conta bloqueada.'))
    bcrypt.compare(candidatePassword, this.password, function(err, match) {
      if (err) return reject(err)
      return resolve(match)
    })
  })
}

UserSchema.methods.downloadPicture = function(url) {
  return new Promise(function(resolve, reject) {
    const user = this
    request({
      method: 'GET',
      url: url,
      encoding: null,
    },
    function(error, response, body) {
      if (error) reject(error)
      user.picture.contentType = response.headers['content-type']
      user.picture.data = new Buffer(body, 'utf-8')
      resolve(user.save())
    })
  })
}

UserSchema.statics.notify = (id, notification) => {
  const Notification = mongoose.model('Notification')
  const User = mongoose.model('User')

  User.findById(id).exec()
      .then((user) => {
        notification.user = user._id
        new Notification(notification).save()
      })
}

module.exports = mongoose.model('User', UserSchema)
