FROM node:lts-alpine

WORKDIR /app

COPY package.json .
COPY yarn.lock .
RUN yarn install --production --frozen-lockfile && \
yarn cache clean

COPY . .

VOLUME /app/ssl
EXPOSE 443
CMD ["yarn", "start"]

