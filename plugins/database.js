const debug = require('debug')('ligacnl:plugins:database')
const mongoose = require('mongoose')

const dbUrl = process.env.DB_URL || 'mongodb://127.0.0.1:27017/ncl'
mongoose.connection.on('open', () =>
  debug(`Successfully connected to database: ${dbUrl}`))
mongoose.connection.on('error', () => {
  debug(`Failed to connect to database: ${dbUrl}`)
  process.exit(1)
})
mongoose.connect(dbUrl, {useNewUrlParser: true})

module.exports = (req, res, next) => {
  next()
}
