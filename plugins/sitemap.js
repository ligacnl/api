const debug = require('debug')('ligacnl:sitemap')
const fs = require('fs')
const path = require('path')
const sitemap = require('sitemap')

const models = require('../models')
const News = models.News
const sitemapFile = path.normalize(`${__dirname}/../data/sitemap.xml`)

async function generate() {
  debug(`Generating sitemap to: ${sitemapFile}`)
  const homeUrl = {
    url: '/',
    changefreq: 'daily',
    priority: 0.8,
  }

  const news = await News.find({}).sort('-createdAt').exec()
  const newsUrls = news.map((item) => ({
    url: `/news/${ item.slug }`,
    lastmod: item.updatedAt,
    changefreq: 'never',
    priority: 1,
  }))

  const sitemapData = sitemap.createSitemap({
    hostname: 'https://www.ligacnl.com.br/',
    urls: [
      homeUrl,
      ...newsUrls,
    ],
  })
  fs.writeFileSync(sitemapFile, sitemapData.toString())
}
setInterval(generate, 86400000)
generate()

module.exports = (req, res) => {
  debug(`Sitemap viewed by: ${ req.headers['user-agent'] }`)
  res.sendFile(sitemapFile)
}
