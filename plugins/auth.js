const debug = require('debug')('ligacnl:auth')
const jwt = require('jsonwebtoken')

const jwtSecret = process.env.SECRET || 'L1!G4Cn{I_7@$'
const jwtOptions = {
  issuer: 'ligacnl.com.br',
}

module.exports.tokenizeUser = (req, user) => {
  debug(`Tokenize user: ${user.name}`)
  return jwt.sign({
    userId: user._id.toString(),
    admin: user.admin,
    organizer: user.organizer,
    editor: user.editor,
  }, jwtSecret, jwtOptions)
}

module.exports.validateToken = function(req, res, next) {
  req.user = {}
  const authorization = req.headers.authorization
  if (!authorization) return next()
  if (authorization.split(' ')[0] !== 'Bearer') return next()

  const token = authorization.split(' ')[1]
  return jwt.verify(token, jwtSecret, jwtOptions, (err, user) => {
    if (err) return res.status(401).send(err)

    req.user = {
      _id: user.userId,
      admin: user.admin || false,
      organizer: user.organizer || false,
      editor: user.editor || false,
    }
    return next()
  })
}
