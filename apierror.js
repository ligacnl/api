module.exports = class APIError extends Error {
  constructor(status = 500, message) {
    super(message)
    this.status = status
    this.message = message
  }
}
