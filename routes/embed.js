const debug = require('debug')('ligacnl:routes:embed')
const express = require('express')
const asyncRoute = require('express-async-handler')
const request = require('request')

const router = new express.Router()
const APIError = require('../apierror')

router.get('/', asyncRoute(async (req, res, next) => {
  const {url} = req.query
  if (!url) throw new APIError(404, 'Não há url para buscar')
  if (!req.query.format) req.query.format = 'json'

  let providers = req.app.get('oembedProviders')
  if (!providers) throw new APIError('Provedores de serviço faltando.')

  providers = providers.filter((provider) => {
    if (url.startsWith(provider.provider_url)) return true
    return provider.endpoints.some((endpoint) => {
      if (!endpoint.schemes) return false
      return endpoint.schemes.some((pattern) => {
        try {
          return new RegExp(pattern.replace('*', '.*')).test(url)
        } catch (err) {
          return false
        }
      })
    })
  })

  if (providers.length === 0) throw new APIError(415, 'Media não suportada.')
  if (providers[0].endpoints[0].discovery) {
    // TODO: handle discovery flow
    // curl url | grep oembed+${req.query.format}
  }
  // TODO: Find best match?
  let api = providers[0].endpoints[0].url
  api = api.replace('{format}', req.query.format)

  request({
    method: 'GET',
    url: api,
    qs: req.query,
    headers: {
      'User-Agent': 'LigaCNL/1.0',
    },
  }, (err, response, body) => {
    if (err) return next(err)
    if (response.statusCode !== 200) return next(new Error(response.statusMessage))
    try {
      const result = JSON.parse(body)
      debug(`Embed: ${result.title || result.url}`)
      return res.send(result)
    } catch (err) {
      return next(err)
    }
  })
}))

module.exports = router
