const debug = require('debug')('ligacnl:routes:admin')
const express = require('express')
const asyncRoute = require('express-async-handler')

const router = new express.Router()
const APIError = require('../apierror')
const models = require('../models')
const User = models.User

router.post('/permissions.json', asyncRoute(async (req, res) => {
  const {id, admin, organizer, editor, bot} = req.body
  // This can be a middleware for the entire admin router if needed
  if (!req.user.admin) {
    throw new APIError(403, 'Não autorizado')
  }

  let user = await User.findById(id).exec()
  if (!user) throw new APIError(404, 'Usuário não encontrado.')
  if (admin != null) user.admin = admin
  if (organizer != null) user.organizer = organizer
  if (editor != null) user.editor = editor
  if (bot != null) user.bot = bot
  user = await user.save()

  debug(`Permissions changed: ${ user.email }`)
  res.send(user)
}))

module.exports = router
