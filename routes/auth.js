const debug = require('debug')('ligacnl:routes:auth')
const express = require('express')
const asyncRoute = require('express-async-handler')
const request = require('request')
const uuid = require('uuid/v1')
const validator = require('validator')

const router = new express.Router()
const auth = require('../plugins/auth')
const mail = require('../plugins/email')
const APIError = require('../apierror')
const ObjectId = require('mongoose').Types.ObjectId
const models = require('../models')
const User = models.User

router.post('/register', asyncRoute(async (req, res) => {
  const {email, name, password} = req.body
  if (!email || !password) {
    throw new APIError(400, 'Preencha todos os campos.')
  } else if (!validator.isEmail(email)) {
    throw new APIError(400, 'E-mail inválido.')
  } else if (!validator.isLength(password, {min: 3})) {
    throw new APIError(400, 'Senha deve possuir no minimo 3 caracteres.')
  }

  const user = await new User({email, name, password}).save()
  const token = auth.tokenizeUser(req, user)
  debug(`New user registered: ${user.email}`)
  return res.send({token})
}))

router.post('/login', asyncRoute(async (req, res) => {
  const {email, password} = req.body
  if (!email || !password) {
    throw new APIError(400, 'Preencha todos os campos.')
  }

  const user = await User.findOne({email}).exec()
  if (!user) throw new APIError(401, 'Usuário ou senha inválidos.')
  if (!await user.checkPassword(password)) {
    throw new APIError(403, 'Usuário ou senha inválidos.')
  }
  const token = auth.tokenizeUser(req, user)
  debug(`User logged in: ${ user.email}`)
  return res.send({token})
}))

router.post('/login/facebook', asyncRoute(async (req, res) => {
  const {accessToken} = req.body
  if (!accessToken) throw new APIError(400, 'Existem campos faltando.')

  return request(`https://graph.facebook.com/v2.9/me?fields=name,email,picture.width(200).height(200)&access_token=${accessToken}`, async (error, response, body) => {
    if (error || !response) {
      throw new APIError(400, 'Não foi possível contactar o Facebook.')
    }
    const fbuser = JSON.parse(body)
    let user = await User.findOne({facebookId: fbuser.id}).exec()
    if (!user) {
      if (!fbuser.name && !fbuser.email) {
        throw new APIError(400, 'Não foi possível recuperar informações '+
        'do perfil Facebook. Por favor, verifique se possui nome e e-mail '+
        'associados à sua conta.')
      }
      user = await new User({
        name: fbuser.name,
        email: fbuser.email,
        facebookId: fbuser.id,
      }).save()
    }

    if (!user) {
      throw new APIError(403, 'Erro ao autenticar. Por favor, tente novamente.')
    }
    await user.downloadPicture(fbuser.picture.data.url)
    const token = auth.tokenizeUser(req, user)
    debug(`User logged in using Facebook API: ${user.email}`)
    return res.send({token})
  })
}))

router.post('/recover', asyncRoute(async (req, res) => {
  const {email} = req.body
  const mailer = mail.mailer
  const mailOpts = mail.mailOptions

  if (!email) throw new APIError(400, 'Preencha todos os campos.')

  let user = await User.findOne({email}).exec()
  if (!user) throw new APIError(404, 'Usuário não encontrado.')
  user.resetToken = uuid()
  user = await user.save()

  mailOpts.to = user.email
  mailOpts.subject = 'LigaCNL - Recuperar Senha'
  // TODO: Template for emails
  mailOpts.html =
`<p>Você ou alguem solicitou uma recuperação de senha.</p>\
<p>Para recuperar sua senha, por favor clique <a href='https://www.ligacnl.com.br/reset?id=${ user.id }&resetToken=${ user.resetToken } target='_blank'>aqui</a>.</p>\
<p>Caso não tenha sido você, por favor, ignore este e-mail.</p><p></p>\
<p>Atenciosamente,</p><p>Equipe LigaCNL.</p>`
  debug(mailOpts.html)

  await new Promise((resolve, reject) => {
    mailer.sendMail(mailOpts, (err, info) => {
      if (err) {
        debug('User recovery failed:', err)
        reject(new APIError(500, 'Não foi possível enviar e-mail de recuperação. Contate um administrador para recuperar sua senha.'))
      }
      return resolve(info)
    })
  })
  debug('User requested email recovery:', user.email)
  return res.sendStatus(200)
}))

router.post('/reset', asyncRoute(async (req, res) => {
  const {id, password, resetToken} = req.body

  if (!id || !password || !resetToken) {
    throw new APIError(400, 'Preencha todos os campos.')
  } else if (!validator.isLength(password, {min: 3})) {
    throw new APIError(400, 'Senha deve possuir no minimo 3 caracteres.')
  } else if (!ObjectId.isValid(id)) {
    throw new APIError(400, 'O id de usuário não está em um formato válido.')
  }

  let user = await User.findOne({_id: id, resetToken}).exec()
  if (!user) throw new APIError(500, 'Este token está expirado ou inválido. Por favor, solicite outra senha.')

  user.password = password
  user.resetToken = null
  user = await user.save()

  const token = auth.tokenizeUser(req, user)
  debug(`Password reset: ${user.email}`)
  return res.send({token})
}))

router.post('/password', asyncRoute(async (req, res) => {
  const {id, password, newPassword} = req.body

  if (!password || !newPassword) {
    throw new APIError(400, 'Preencha todos os campos.')
  } else if (!validator.isLength(newPassword, {min: 3})) {
    throw new APIError(400, 'Senha deve possuir no minimo 3 caracteres.')
  }

  let user = await User.findById(id).exec()
  if (!user) throw new APIError(404, 'Usuário não encontrado.')
  if (user._id != req.user._id && !req.user.admin) throw new APIError(403, 'Ação não permitida')

  if (!await user.checkPassword(password) && !req.user.admin) {
    throw new APIError(403, 'Senha atual incorreta.')
  }
  user.password = newPassword
  user = await user.save()

  debug(`Password changed: ${user.email}`)
  res.sendStatus(200)
}))

router.post('/connect/facebook', asyncRoute(async (req, res) => {
  if (!req.user._id) {
    throw new APIError(403, 'Não autorizado.')
  }
  const {accessToken} = req.body
  if (!accessToken) throw new APIError(400, 'Existem campos faltando.')

  return request(`https://graph.facebook.com/v2.9/me?fields=name,email,picture.width(200).height(200)&access_token=${accessToken}`, async (error, response, body) => {
    if (error || !response) {
      throw new APIError(400, 'Não foi possível contactar Facebook.')
    }
    const fbuser = JSON.parse(body)
    if (await User.find({facebookId: fbuser.id}).exec()) {
      throw new Error('Já existe um usuário conectado a esta conta.')
    }
    const user = await User.findByIdAndUpdate(
        req.user._id,
        {$set: {facebookId: fbuser.id}},
        {new: true}
    )
    debug(`User connected account to Facebook API: ${user.email}`)
    return res.send(user)
  })
}))

router.get('/disconnect/facebook', asyncRoute(async (req, res) => {
  if (!req.user._id) {
    throw new APIError(403, 'Não autorizado.')
  }

  const user = User.findByIdAndUpdate(
      req.user._id,
      {$unset: {facebookId: ''}},
      {new: true}
  )
  if (!user) {
    throw new APIError(403, 'Erro ao atualizar o usuário. '+
                       'Por favor, faça o login novamente')
  }
  debug(`User disconnected from Facebook API: ${user.email}`)
  return res.send(user)
}))

module.exports = router
