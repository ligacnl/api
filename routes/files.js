const debug = require('debug')('ligacnl:routes:files')
const express = require('express')
const asyncRoute = require('express-async-handler')
const multiparty = require('connect-multiparty')()
const fs = require('fs')
const path = require('path')

const router = new express.Router()
const APIError = require('../apierror')

router.post('/upload', multiparty, asyncRoute(async (req, res) => {
  if (!req.user.admin && !req.user.organizer && !req.user.editor) {
    throw new APIError(403, 'Não autorizado.')
  }

  const CDN_URL = req.app.get('cdn')
  const s3 = req.app.get('s3')
  const file = req.files.file
  const uuid = require('uuid/v1')()

  const params = {
    Body: fs.createReadStream(file.path),
    Bucket: 'ligacnl/uploads',
    Key: uuid + path.extname(file.path),
    ContentLength: file.size,
    ContentType: file.type,
  }
  s3.putObject(params, (err, data) => {
    fs.unlink(file.path, (err) => {
      if (err) debug('Error deleting temp file [', file.path, ']:', err)
    })
    if (err) throw new APIError(500, 'Não foi possível fazer upload de arquivo.')
    return res.send(`${ CDN_URL }uploads/${ uuid }${ path.extname(file.path) }`)
  })
}))

module.exports = router
