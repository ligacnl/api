const debug = require('debug')('ligacnl:routers:users')
const express = require('express')
const asyncRoute = require('express-async-handler')
const path = require('path')
const multiparty = require('connect-multiparty')()
const fs = require('fs')
const validator = require('validator')

const router = new express.Router()
const APIError = require('../apierror')
const models = require('../models')
const User = models.User

router.get('/', asyncRoute(async (req, res) => {
  const {q, bots} = req.query
  let {limit, skip, sort} = req.query
  limit = parseInt(limit) || 10
  skip = parseInt(skip) || 0
  sort = (sort || 'id').replace('id', '_id')

  let query = User.find()
      .limit(limit)
      .skip(skip)
      .sort(sort)
  if (bots != null) {
    query = query.where({bots})
  }
  if (q != null) {
    query = query.or([
      {_id: q},
      {name: new RegExp(q, 'i')},
      {email: q},
    ])
  }
  const users = await query.exec()
  debug(`List ${ users.length } users. Query: ${ req.url }`)
  return res.send(users)
}))

router.get('/:id', asyncRoute(async (req, res) => {
  let {id} = req.params
  if (id == 'me') {
    id = req.user._id
  }

  const user = await User.findById(id).exec()
  if (!user) throw new APIError(404, 'Usuário não encontrado.')
  debug(`Profile viewed: ${user.email}`)
  return res.send(user)
}))

router.put('/:id', asyncRoute(async (req, res) => {
  const {id} = req.params
  const body = req.body

  if (!body) {
    throw new APIError(400, 'Preencha todos os campos.')
  } else if (body.email && !validator.isEmail(body.email)) {
    throw new APIError(400, 'E-mail inválido.')
  }

  let user = await User.findById(id).exec()
  if (!user) throw new APIError(404, 'Usuário não encontrado.')
  if (user._id != req.user._id && !req.user.admin) throw new APIError(403, 'Ação não permitida')

  user = Object.assign(user, body)
  if (body.email != null) {
    user.emailVerified = false
  }
  user = await user.save()
  debug(`Profile edited: ${user.email}`)

  return res.send(user)
}))

router.get('/:id/avatar', asyncRoute(async (req, res) => {
  const {id} = req.params

  const user = await User.findById(id, 'picture').exec()
  if (user && user.picture && user.picture.data) res.contentType(user.picture.contentType).send(user.picture.data)
  else res.sendFile(path.join(__dirname, '../public/images', 'default_avatar.png'))
}))

router.post('/:id/avatar', multiparty, asyncRoute(async (req, res) => {
  const {id} = req.params
  const {file} = req.files

  let user = await User.findById(id).exec()
  if (!user) throw new APIError(404, 'Usuário não encontrado.')
  if (user._id != req.user._id && !req.user.admin) throw new APIError(403, 'Ação não permitida')
  user.picture.data = fs.readFileSync(file.path)
  user.picture.contentType = file.type
  fs.unlink(file.path, (err) => {
    if (err) debug(`Error deleting tmp file [${ file.path }]:`, err)
  })
  user = await user.save()
  debug(`Picture changed for user: ${ user.email }`)
  return res.send(user)
}))

module.exports = router
