const debug = require('debug')('ligacnl:routes:news')
const express = require('express')
const asyncRoute = require('express-async-handler')

const router = new express.Router()
const APIError = require('../apierror')
const ObjectId = require('mongoose').Types.ObjectId
const models = require('../models')
const News = models.News

router.get('/', asyncRoute(async (req, res) => {
  const {carousel} = req.query
  let {limit, skip, sort, tags} = req.query
  limit = parseInt(limit) || 30
  skip = parseInt(skip) || 0
  sort = (sort || '-createdAt').replace('id', '_id')

  let query = News.find()
      .populate('author')
      .limit(limit)
      .skip(skip)
      .sort(sort)
  if (carousel != null) {
    query = query.where({carousel})
  }
  if (tags != null) {
    tags = tags.split(',') || tags
    query = query.where({tags: {$in: tags}})
  }
  const news = await query.exec()
  debug(`List ${ news.length } news. Query: ${ req.url }`)
  return res.send(news)
}))

router.post('/', asyncRoute(async (req, res) => {
  const body = req.body

  if (!req.user.admin && !req.user.editor && !req.user.organizer) {
    throw new APIError(403, 'Não autorizado.')
  }
  if (!body || !body.title || !body.content) {
    throw new APIError(400, 'Preencha todos os campos.')
  }

  if (!body.author) body.author = req.user._id
  if (!body.game) body.game = 'all'

  let news = await News.findById(body._id).exec()
  if (!news) news = await new News(body).save()
  else {
    if (news.author != req.user._id && !req.user.admin) throw new APIError(403, 'Ação não permitida')

    // TODO: Find a way to write this code less uglier
    if (body.title != null) news.title = body.title
    if (body.description != null) news.description = body.description
    if (body.tags != null) news.tags = body.tags
    if (body.game != null) news.game = body.game
    if (body.image != null) news.image = body.image
    if (body.content != null) news.content = body.content
    if (body.carousel != null) news.carousel = body.carousel
    if (body.tournament != null) news.tournament = body.tournament
    if (body.league != null) news.league = body.league

    news = await news.save()
  }
  news = await News.populate(news, 'author')
  debug(`News upsert: ${ news.title }`)
  return res.send(news)
}))

router.get('/:id', asyncRoute(async (req, res, next) => {
  const {id} = req.params
  if (!ObjectId.isValid(id)) return next()

  const news = await News.findById(id).populate('author').exec()
  if (!news) return next()

  debug(`News viewed: ${ news.title }`)
  return res.send(news)
}))

router.get('/:slug', asyncRoute(async (req, res) => {
  const {slug} = req.params

  const news = await News.findOne({slug}).populate('author').exec()
  if (!news) throw new APIError(404, 'Notícia não encontrada.')
  debug(`News viewed: ${ news.title }`)
  return res.send(news)
}))

router.delete('/:id', asyncRoute(async (req, res, next) => {
  const {id} = req.params

  if (!req.user.admin && !req.user.editor && !req.user.organizer) {
    throw new APIError(403, 'Não autorizado.')
  }

  let news = await News.findById(id).exec()
  if (!news) throw new APIError(404, 'Notícia não encontrada.')
  if (news.author != req.user._id && !req.user.admin) throw new APIError(403, 'Ação não permitida')

  news = await news.remove()
  debug(`News removed: ${ news.title }`)
  res.sendStatus(200)
}))

module.exports = router
